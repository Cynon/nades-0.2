package com.cwat.util;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by alexthornburg on 12/9/13.
 */
public class UniversalHTTP extends AsyncTask<String,Void,JSONArray>{

    JSONArray jArray;
    public UniversalHTTP(){
    }


    @Override
    protected JSONArray doInBackground(String... server) {
        String result = "";


        try{
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(server[0]);
            HttpResponse executed = httpclient.execute(httppost);
            HttpEntity entity = executed.getEntity();

            InputStream is = entity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result=sb.toString();
            jArray = new JSONArray(result);
            reader.close();
            httppost.abort();
            return jArray;


        }catch(Exception e){
            Log.i("Error", "Json error");
        }


        return null;
    }


    protected void onPostExecute(){
        super.onPostExecute(jArray);
    }


}
